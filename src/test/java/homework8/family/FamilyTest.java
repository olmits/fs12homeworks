package homework8.family;

import homework8.family.member.Human;
import homework8.family.member.Man;
import homework8.family.member.Woman;
import homework8.family.pet.Dog;
import homework8.family.pet.DomesticCat;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {
//    TEST FAMILY
    private final Human mom = new Woman("Gloria", "Tribbiani", 1939, 199);
    private final Human dad = new Man("Joseph Sr.", "Tribbiani", 1942, 199);
    private final Human kid1 = new Man("Joseph Jr.", "Tribbiani", 1970, 99);
    private final Human kid2 = new Woman("Gina.", "Tribbiani", 1980, 199);
    private final Dog dog = new Dog("Dog", 3,20, new HashSet<>(Arrays.asList("eating", "barking")));
    private final DomesticCat cat = new DomesticCat("Cat", 5,80, new HashSet<>(Arrays.asList("scratch", "mrr")));
    private final Family family = new Family(mom, dad);


    @Test
    void addChild() {
        assertEquals(2, family.countFamily());
        family.addChild(kid1);
        assertEquals(3, family.countFamily());
    }

    @Test
    void deleteChildByIndex1() {
        family.addChild(kid1);
        family.addChild(kid2);
        family.deleteChild(1);
        assertEquals(3, family.countFamily());
        assertEquals("Joseph Jr.", family.getChildren().get(0).getName());
    }

    @Test
    void deleteChildByIndex2() {
        family.addChild(kid1);
        family.addChild(kid2);
        family.deleteChild(2);
        assertEquals(4, family.countFamily());
    }

    @Test
    void deleteChildByHuman3() {
        family.addChild(kid1);
        family.addChild(kid2);
        family.deleteChild(kid2);
        assertEquals(3, family.countFamily());
        assertEquals("Joseph Jr.", family.getChildren().get(0).getName());
    }

    @Test
    void deleteChildByHuman4() {
        family.addChild(kid1);
        family.deleteChild(kid2);
        assertEquals(3, family.countFamily());
    }

    @Test
    void countFamily() {
        assertEquals(2, family.countFamily());
    }

    @Test
    void testToString() {
        family.addChild(kid1);
        family.addChild(kid2);
        family.addPet(dog);
        family.addPet(cat);
        String test = "Family{mother={Gloria Tribbiani}, father={Joseph Sr. Tribbiani}, children={[Joseph Jr. Tribbiani, Gina. Tribbiani]}, pets={[DOG Dog, DOMESTIC_CAT Cat]}}";
        System.out.println(family.toString());
        assertEquals(test, family.toString());
    }

    @Test
    void testHumanFamilyLink() {
        family.addChild(kid1);
        assertEquals(family, kid1.getFamily());
        assertNotEquals(family, kid2.getFamily());
        assertEquals(family, mom.getFamily());
        assertEquals(family, dad.getFamily());
    }

    @Test
    void testPetHumanLinkThroughFamily() {
        family.addPet(dog);
        family.addChild(kid1);
        assertTrue(kid1.getFamily().getPets().contains(dog));
        assertTrue(mom.getFamily().getPets().contains(dog));
        assertFalse(dad.getFamily().getPets().contains(cat));
    }
}