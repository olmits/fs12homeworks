package homework8.service;

import homework8.controller.FamilyController;
import homework8.family.Family;
import homework8.family.member.Human;
import homework8.family.member.Man;
import homework8.family.member.Woman;
import homework8.family.pet.Dog;
import homework8.family.pet.DomesticCat;
import homework8.family.pet.Pet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(MockitoJUnitRunner.class)
class FamilyServiceTest {

    private final Woman woman1 = new Woman("Gloria", "Tribbiani", 1939, 199);
    private final Woman woman2 = new Woman("Sandra", "Green", 1939, 199);

    private final Man man1 = new Man("Joseph Sr.", "Tribbiani", 1942, 199);
    private final Man man2 = new Man("Leonard", "Green", 1942, 199);

    private final Man kid1 = new Man("Joseph Jr.", "Tribbiani", 1970, 99);
    private final Woman kid2 = new Woman("Gina.", "Tribbiani", 1980, 199);
    private final Dog dog = new Dog("Dog", 3,20, new HashSet<>(Arrays.asList("eating", "barking")));
    private final DomesticCat cat = new DomesticCat("Cat", 5,80, new HashSet<>(Arrays.asList("scratch", "mrr")));

    private FamilyController familyController;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    @BeforeEach
    public void setUp() {
        familyController = new FamilyController();
        familyController.createNewFamily(woman1, man1);
        familyController.createNewFamily(woman2, man2);

        Family family1 = familyController.getAllFamilies().get(0);

        familyController.adoptChild(family1, kid1);

        familyController.addPet(0, dog);

        System.setOut(new PrintStream(outputStreamCaptor));
    }

    @Test
    void getAllFamilies() {
        FamilyController familyController = new FamilyController();
        familyController.createNewFamily(woman1, man1);
        List<Family> families = familyController.getAllFamilies();

        Family family = new Family(woman1, man1);
        List<Family> test = Collections.singletonList(family);
        assertEquals(test, families);
    }

    @Test
    void getFamiliesBiggerThan() {
        familyController.getFamiliesBiggerThan(2);

        String test = ">>> Families with a number of members bigger than 2:\n" +
                "Family{mother={Gloria Tribbiani}, father={Joseph Sr. Tribbiani}, children={[Joseph Jr. Tribbiani]}, pets={[DOG Dog]}}";


        assertEquals(test, outputStreamCaptor.toString().trim());
    }

    @Test
    void getFamiliesLessThan() {
        familyController.getFamiliesLessThan(3);

        String test = ">>> Families with a number of members less than 3:\n" +
                "Family{mother={Sandra Green}, father={Leonard Green}, children={[]}, pets={no pet}}";

        assertEquals(test, outputStreamCaptor.toString().trim());
    }

    @Test
    void createNewFamily() {
        FamilyController familyController = Mockito.mock(FamilyController.class);
        familyController.createNewFamily(woman1, man1);

        Mockito.verify(familyController, Mockito.times(1)).createNewFamily(woman1, man1);
    }

    @Test
    void bornChild() {
        Family family2 = familyController.getAllFamilies().get(1);
        familyController.bornChild(family2, "Alex", "Dianna");
        List<Human> children = family2.getChildren();
        String test = children.get(0).getName();

        assertEquals(3, family2.countFamily());
        assertTrue(test.equals("Alex") || test.equals("Dianna"));
    }

    @Test
    void adoptChild() {
        Family family1 = familyController.getAllFamilies().get(0);
        familyController.adoptChild(family1, kid2);

        String test = "[Human{name='Joseph Jr.', surname='Tribbiani', year=1970, iq=99, , Human{name='Gina.', surname='Tribbiani', year=1980, iq=199, ]";

        assertEquals(4, family1.countFamily());
        assertEquals(test, family1.getChildren().toString());
    }

    @Test
    void deleteAllChildrenOlderThen() {
        Family family2 = familyController.getAllFamilies().get(1);
        familyController.bornChild(family2, "Alex", "Alex");

        familyController.deleteAllChildrenOlderThen(10);

        String test = "[Family{mother={Gloria Tribbiani}, father={Joseph Sr. Tribbiani}, children={[]}, pets={[DOG Dog]}}, Family{mother={Sandra Green}, father={Leonard Green}, children={[Alex Green]}, pets={no pet}}]";

        assertEquals(test, familyController.getAllFamilies().toString());
    }

    @Test
    void count() {
        assertEquals(2, familyController.count());
    }

    @Test
    void getPets() {
        Set<Pet> pets = new HashSet<>(Collections.singletonList(dog));
        assertEquals(pets, familyController.getPets(0));
    }

    @Test
    void addPet() {
        familyController.addPet(1, cat);
        assertTrue(familyController.getPets(1).contains(cat));
    }
}