package homework1;

import java.util.Random;
import java.util.Scanner;
import java.util.Arrays;
import java.util.stream.IntStream;

public class Homework1 {

    /**
     *
     * @return randomly generated int
     */
    private static int getRandomNumber() {
        Random r = new Random();
        return 1 + r.nextInt(100);
    }

    private static final int randomNumber = getRandomNumber();

    /**
     *
     * @param s - String input that should be printed in console
     */
    private static void print(String s) {
        System.out.print(s);
    }

    private static Scanner scanner = new Scanner(System.in);

    /**
     *
     * @param s - String that should be verified on not-a-number value contains
     * @return Boolean: true if String is a number, false if String cannot be converted into a number
     */
    private static boolean isInteger(String s) {
        Scanner sc = new Scanner(s.trim());
        if (!sc.hasNextInt(10)) return false;
        sc.nextInt(10);
        return !sc.hasNext();
    }

    private static int[] addNumberToArray(int [] arr, int a) {
        int [] result = Arrays.copyOf(arr, arr.length + 1);
        result[arr.length] = a;
        return result;
    }

    public static void main(String[] args) {
        print("Let the game begin!\nPlayer name: ");
        String name = scanner.next();
        print("Enter a number: ");

        int[] inputs = new int[] { };

        while (IntStream.of(inputs).noneMatch(x -> x == randomNumber)) {

            String x = scanner.next();
            if (!isInteger(x)) continue;
            int xInt = Integer.parseInt(x);
            if (xInt > randomNumber) print("Your number is too big. Please, try again.\n");
            if (xInt < randomNumber) print("Your number is too small. Please, try again.\n");

            inputs = addNumberToArray(inputs, xInt);
            Arrays.sort(inputs);

            print(String.format("Your numbers: %s\n", Arrays.toString(inputs)));
        }

        print(String.format("Number %d\nCongratulations, %s!", randomNumber, name));
    }
}
