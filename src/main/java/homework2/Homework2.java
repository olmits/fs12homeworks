package homework2;

import java.util.Arrays;
import java.util.Optional;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Homework2 {

    private static void print(String s) {
        System.out.print(s);
    }

    /**
     * Print a landfill passed as param
     * @param landfill - int[][] representing a landfill
     */
    private static void printLandfill(int [][] landfill) {
        print("\n");
        IntStream.range(0, landfill.length + 1).forEach(i -> print(String.format(" %d |", i)));
        IntStream.range(0, landfill.length).forEach(i -> {
            String points = Arrays
                    .stream(landfill[i])
                    .mapToObj(j -> {
                        if (j == 1) return "x";
                        else if (j == -1) return "*";
                        else return "-";
                    })
                    .collect(Collectors.joining(" | "));
            print(String.format("\n %d | %s |", i + 1, points));
        });
    }

    private static int getRandomIntInRange() {
        return (int) ((Math.random() * (5 - 1)) + 1);
    }

    /**
     * Verify if pair of x and y of landfill as [x, y] contains in shoots
     * @param xPoint - x/i coordinate
     * @param yPoint - y/j coordinate
     * @return boolean
     */
    private static boolean checkLandfillPointInShoots(int xPoint, int yPoint) {
        return Arrays.stream(shoots).anyMatch(obj -> obj[0] == (xPoint) && obj[1] == (yPoint));
    }

    /**
     * Verify if pair of x of landfill as [x, y] contains in shoots AND is targetX and targetY
     * @param xPoint - x/i coordinate
     * @param yPoint - y/j coordinate
     * @return boolean
     */
    private static boolean checkTargetPointInShoots(int xPoint, int yPoint) {
        return Arrays.stream(shoots).anyMatch(obj -> obj[0] == (xPoint)
                && obj[1] == (yPoint)
                && obj[0] == targetX
                && obj[1] == targetY
        );
    }

    /**
     * Generate 5x5 landfill that includes 0 (not shoot), -1 (shoot, not a target) or 1 (shoot, a target)
     * @return int[][] of coordinate pairs representing matrix 5x5
     */
    private static int[][] generateLandfill() {
        return IntStream
                .range(0, 5)
                .mapToObj(i -> IntStream.range(0, 5)
                                        .map(j -> {
                                            if (checkTargetPointInShoots(i, j)) return 1;
                                            else if (checkLandfillPointInShoots(i, j)) return -1;
                                            else return 0;
                                        })
                                        .toArray())
                .toArray(int[][]::new);
    }

    /**
     * Add x an y pair of coordinate
     * @param shoot - int[2] with x and y coordinates
     */
    private static void addShootToShoots(int[] shoot) {
        int size = shoots.length;
        shoots = Arrays.copyOf(shoots, size + 1);
        shoots[size] = shoot;
    }

    private final static int targetX = getRandomIntInRange();
    private final static int targetY = getRandomIntInRange();
    private static int[][] shoots = {};
    private static int[][] landfill = generateLandfill();

    private static Optional<Integer> toInt(String line) {
        try {
            int x = Integer.parseInt(line);
            return Optional.of(x);
        } catch (NumberFormatException x) {
            return Optional.empty();
        }
    }

    /**
     *
     * @param sc - Scanner prop
     * @return int
     */
    private static int waitForInt(Scanner sc) {
        String line = sc.next();
        Optional<Integer> r = toInt(line);
        if (!r.isPresent()) {
            print("Input should be a number in range from 1 to 5 inclusive.");
            return waitForInt(sc);
        }
        return r.get();
    }

    public static void main(String[] args) {
        print(String.format("%d - %d\n", targetX, targetY));
        try {
            do {
                printLandfill(landfill);
                if (shoots.length == 0) {
                    print("\nAll set. Get ready to rumble!\n");
                } else {
                    print("\nAnother try\n");
                }

                Scanner sc = new Scanner(System.in);
                print("Enter X coordinate: \n");
                int x = waitForInt(sc);
                print("Enter Y coordinate: \n");
                int y = waitForInt(sc);
                addShootToShoots(new int[]{x - 1, y - 1});
                landfill = generateLandfill();
            } while (!checkLandfillPointInShoots(targetX, targetY));

            printLandfill(landfill);
            print("\nYou have won!");
        }
        catch (ClassCastException res) {
            print(res.getMessage());
        }
    }
}
