package homework8;

import homework8.family.*;
import homework8.family.member.Human;
import homework8.family.member.Man;
import homework8.family.member.Sex;
import homework8.family.member.Woman;
import homework8.family.pet.Dog;
import homework8.family.pet.Pet;

import java.util.Arrays;
import java.util.HashSet;

public class Homework4 {
    public static void main(String[] args) {
//        FAMILY 1
        Human mom1 = new Woman("Naomi", "Scott", 1992);
        Human dad1 = new Man();
        Family family1 = new Family(mom1, dad1);

        Human man1 = new Man();
        man1.setName("Jordan");
        man1.setSurname("Spence");
        man1.setHumanSex(Sex.MALE);
        man1.setYearOfBirthday(1982);
        man1.setIq(1);
        man1.setFamily(family1);

//        FAMILY 2
        Human mom2 = new Woman("Jacqueline", "Carrault", 1992, 150);
        Human dad2 = new Man("Maximilien", "Robespierre", 1758, 149);
        Family family2 = new Family(mom2, dad2);

//        FAMILY 3
        Human mom3 = new Woman("Frances", "Cain" , 1950, 134);
        Human dad3 = new Man("Jeremy", "Clarkson", 1960, 134);
        Family family3 = new Family(mom3, dad3);

//        FAMILY 4
        Human mom4 = new Woman("Gloria", "Tribbiani", 1939, 199);
        Human dad4 = new Man("Joseph Sr.", "Tribbiani", 1942, 199);
        Human kid4 = new Man("Joseph Jr.", "Tribbiani", 1970, 99);
        Pet dog4 = new Dog( "Dog", 3,20, new HashSet<>(Arrays.asList("eating", "barking")));
        Family family4 = new Family(mom4, dad4);

        family4.addChild(kid4);
        family4.addPet(dog4);


        System.out.printf("" +
                        "FAMILY 1: %s\n>>> MOTHER is %s\n>>> FATHER is %s\n" +
                        "FAMILY 2: %s\n>>> MOTHER is %s\n>>> FATHER is %s\n" +
                        "FAMILY 3: %s\n>>> MOTHER is %s\n>>> FATHER is %s\n" +
                        "FAMILY 4: %s\n>>> MOTHER is %s\n>>> FATHER is %s\n>>> CHILDREN are %s",
                family1, family1.getMother(), family1.getFather(),
                family2, family2.getFather(), family2.getMother(),
                family3, family3.getFather(), family3.getMother(),
                family4, family4.getFather(), family4.getMother(), family4.getChildren().toString());
    }
}
