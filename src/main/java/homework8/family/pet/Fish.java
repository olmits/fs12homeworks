package homework8.family.pet;

import java.util.Set;

public class Fish extends Pet {
    public Fish(String nickname, int age, int cunning, Set<String> habits) {
        super(nickname, age, cunning, habits);
        this.setSpecie(Species.FISH);
    }

    public Fish(String nickname) {
        super(nickname);
        this.setSpecie(Species.FISH);
    }

    public Fish() {
        super();
        this.setSpecie(Species.FISH);
    }

    @Override
    public void respond() {
        System.out.printf("Hello, Master. I'm %s. Please, clear my aquarium!\n", this.getNickname());
    }
}
