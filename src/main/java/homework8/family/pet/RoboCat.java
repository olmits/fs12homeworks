package homework8.family.pet;

import java.util.Set;

public class RoboCat extends Pet implements PetFoolishness {
    public RoboCat(String nickname, int age, int cunning, Set<String> habits) {
        super(nickname, age, cunning, habits);
        this.setSpecie(Species.ROBO_CAT);
    }

    public RoboCat(String nickname) {
        super(nickname);
        this.setSpecie(Species.ROBO_CAT);
    }

    public RoboCat() {
        super();
        setSpecie(Species.ROBO_CAT);
    }

    @Override
    public void respond() {
        System.out.printf("01001001 00100000 01100001 01101101 %s 00100001\n", this.getNickname());
    }

    @Override
    public void fool() {
        System.out.print("404\n");
    }
}
