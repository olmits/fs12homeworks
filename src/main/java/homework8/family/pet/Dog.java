package homework8.family.pet;

import java.util.Set;

public class Dog extends Pet implements PetFoolishness{
    public Dog(String nickname, int age, int cunning, Set<String> habits) {
        super(nickname, age, cunning, habits);
        this.setSpecie(Species.DOG);
    }

    public Dog(String nickname) {
        super(nickname);
        this.setSpecie(Species.DOG);
    }

    public Dog() {
        super();
        this.setSpecie(Species.DOG);
    }

    @Override
    public void respond() {
        System.out.printf("Hey, Master! I'm %s. Am I a good boy?\n", this.getNickname());
    }

    @Override
    public void fool() {
        System.out.print("I should cover it up...\n");
    }
}
