package homework8.family.pet;

public enum Species {
        DOG,
        FISH,
        DOMESTIC_CAT,
        ROBO_CAT,
        UNDEFINED
        }
