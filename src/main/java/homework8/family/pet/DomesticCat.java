package homework8.family.pet;

import java.util.Set;

public class DomesticCat extends Pet implements PetFoolishness{
    public DomesticCat(String nickname, int age, int cunning, Set<String> habits) {
        super(nickname, age, cunning, habits);
        this.setSpecie(Species.DOMESTIC_CAT);
    }

    public DomesticCat(String nickname) {
        super(nickname);
        this.setSpecie(Species.DOMESTIC_CAT);
    }

    public DomesticCat() {
        super();
        this.setSpecie(Species.DOMESTIC_CAT);
    }

    @Override
    public void respond() {
        System.out.printf("I'm %s. I am your Master! Go clear the tray\n", this.getNickname());
    }

    @Override
    public void fool() {
        System.out.print("I should show it up...\n");
    }
}
