package homework8.family.pet;

import java.util.HashSet;
import java.util.Set;

public abstract class Pet {
    private Species specie;
    private String nickname;
    private int age;
    private int cunning;
    private Set<String> habits;

    Pet(String nickname, int age, int cunning, Set<String> habits) {
        this.nickname = nickname;
        this.age = age;
        this.cunning = cunning;
        this.habits = habits;
    }

    Pet(String nickname) {
        this.nickname = nickname;
        this.age = 0;
        this.cunning = 0;
        this.habits = new HashSet<>();
    }

    public Pet() {
        this.specie = Species.UNDEFINED;
        this.nickname = "";
        this.age = 0;
        this.cunning = 0;
        this.habits = new HashSet<>();
    }

    void setSpecie(Species specie) {
        this.specie = specie;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setCunning(int cunning) {
        this.cunning = cunning;
    }

    public void setHabits(Set<String> habits) {
        this.habits = habits;
    }

    public Species getSpecie() {
        return this.specie;
    }

    public String getNickname() {
        return this.nickname;
    }

    public int getAge() {
        return this.age;
    }

    public int getCunning() {
        return this.cunning;
    }

    public Set<String> getHabits() {
        return this.habits;
    }

    public void eat() {
        System.out.print("I'm eating!\n");
    }

    public abstract void respond();

    @Override
    public String toString() {
        return String.format("%s{nickname='%s', age=%d, trickLevel=%d, habits=%s}",
                this.specie,
                this.nickname,
                this.age,
                this.cunning,
                this.habits.toString()
        );
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Pet)) return false;

        Pet x = (Pet) obj;

        boolean nameEquality = this.getNickname().equals(x.getNickname());
        boolean cunningEquality = this.getCunning() == x.getCunning();

        Set<String> thisHabits = this.getHabits();
        Set<String> xHabits = x.getHabits();

        boolean habitsEquality = thisHabits.containsAll(xHabits) && xHabits.containsAll(thisHabits);

        return nameEquality && habitsEquality && cunningEquality;
    }

    protected void finalize() {
        System.out.print(this);
    }
}
