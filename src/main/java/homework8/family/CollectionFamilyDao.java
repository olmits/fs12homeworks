package homework8.family;

import homework8.dao.FamilyDao;
import homework8.family.member.Human;
import homework8.family.member.Man;
import homework8.family.member.Woman;
import homework8.family.pet.Pet;

import java.util.*;
import java.util.stream.Collectors;

public class CollectionFamilyDao implements FamilyDao {
    private List<Family> families = new ArrayList<>();

    private boolean checkIfFamilyExists(Family family) {
        return this.families.contains(family);
    }

    private int getCurrentYear() {
        return Calendar.getInstance().get(Calendar.YEAR);
    }


    @Override
    public List<Family> getAllFamilies() {
        return this.families;
    }

    @Override
    public Family getFamilyByIndex(long id) {
        return this.families.get((int) id);
    }

    @Override
    public boolean deleteFamily(long id) {
        if (id > this.families.size()) return false;
        this.families.remove((int) id);
        return true;
    }

    @Override
    public boolean deleteFamily(Family family) {
        if (!checkIfFamilyExists(family)) return false;
        this.families.remove(family);
        return true;
    }

    @Override
    public void saveFamily(Family family) {
        if (!checkIfFamilyExists(family)) {
            this.families.add(family);
        }
//        TODO: next part of the code has no sense
        int index = this.families.indexOf(family);
        this.families.set(index, family);
    }

    public void getFamiliesBiggerThan(int number) {
        System.out.printf(">>> Families with a number of members bigger than %d:\n", number);
        this.families.stream()
                .filter(x -> x.countFamily() > number)
                .collect(Collectors.toList())
                .forEach(System.out::println);
    }

    public void getFamiliesLessThan(int number) {
        System.out.printf(">>> Families with a number of members less than %d:\n", number);
        this.families.stream()
                .filter(x -> x.countFamily() < number)
                .collect(Collectors.toList())
                .forEach(System.out::println);
    }

    public void createNewFamily(Woman woman, Man man) {
        Family family = new Family(woman, man);
        this.families.add(family);
    }

    public void bornChild(Family family, String boyName, String girlName) {
        if (checkIfFamilyExists(family)) {
            Random r = new Random();
            int value = r.nextInt(11);

            String surname = family.getFather().getSurname();
            int birthday = getCurrentYear();

            if (value > 5) {
                Woman girl = new Woman(girlName, surname, birthday);
                family.addChild(girl);
            } else {
                Man boy = new Man(boyName, surname, birthday);
                family.addChild(boy);
            }

            saveFamily(family);
        }
    }

    public void adoptChild(Family family, Human kid) {
        family.addChild(kid);
        saveFamily(family);
    }

    public void deleteAllChildrenOlderThen(int age) {
        int year = getCurrentYear() - age;
        this.families.forEach(family -> {
            List<Human> agedKids = family.getChildren().stream()
                    .filter(k -> k.getYearOfBirthday() < year)
                    .collect(Collectors.toList());
            agedKids.forEach(family::deleteChild);
            saveFamily(family);
        });
    }

    public int count() {
        return this.families.size();
    }

    public Set<Pet> getPets(long id) {
        if (id > this.families.size()) return null;
        return this.families.get((int) id).getPets();
    }

    public void addPet(long id, Pet pet) {
        if (id < this.families.size()){
            this.families.get((int) id).addPet(pet);
            this.families.get((int) id).addPet(pet);
        };
    }
}
