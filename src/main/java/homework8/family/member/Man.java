package homework8.family.member;

public final class Man extends Human {
    public Man(String name, String surname, int yearOfBirthday, int iq) {
        super(name, surname, yearOfBirthday, iq);
        this.setHumanSex(Sex.MALE);
    }

    public Man(String name, String surname, int yearOfBirthday) {
        super(name, surname, yearOfBirthday);
        this.setHumanSex(Sex.MALE);
    }

    public Man() {
        super();
        this.setHumanSex(Sex.MALE);
    }

    public void repairCar() {
        System.out.printf("%s is repairing his car!\n", this.getName());
    }

    @Override
    void greetPets() {
        this.getFamily().getPets().forEach(x -> {
            System.out.printf("Sup, %s %s?\n", x.getSpecie(), x.getNickname());
        });
    }
}
