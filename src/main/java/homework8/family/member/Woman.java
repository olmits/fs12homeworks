package homework8.family.member;

public final class Woman extends Human {
    public Woman(String name, String surname, int yearOfBirthday, int iq) {
        super(name, surname, yearOfBirthday, iq);
        this.setHumanSex(Sex.FEMALE);
    }

    public Woman(String name, String surname, int yearOfBirthday) {
        super(name, surname, yearOfBirthday);
        this.setHumanSex(Sex.FEMALE);
    }

    public Woman() {
        super();
        this.setHumanSex(Sex.FEMALE);
    }

    public void makeUp() {
        System.out.printf("%s is doing her makeup!\n", this.getName());
    }

    @Override
    public void greetPets() {
        this.getFamily().getPets().forEach(x -> {
            System.out.printf("Hello my dear %s %s\n", x.getSpecie(), x.getNickname());
        });
    }
}
