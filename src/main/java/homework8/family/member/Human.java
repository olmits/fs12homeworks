package homework8.family.member;

import homework8.family.Family;
import homework3.Days;

import java.util.HashMap;
import java.util.Map;

public abstract class Human {
    private Family family;
    private String name;
    private String surname;
    private Sex sex;
    private int yearOfBirthday;
    private int iq = 0;
    private Map<Days, String> schedule = new HashMap<>();

    public Human(String name, String surname, int yearOfBirthday, int iq) {
        this.name = name;
        this.surname = surname;
        this.yearOfBirthday = yearOfBirthday;
        this.iq = iq;
    }

    public Human(String name, String surname, int yearOfBirthday) {
        this.name = name;
        this.surname = surname;
        this.yearOfBirthday = yearOfBirthday;
    }

    public Human() {
        this.name = "";
        this.surname = "";
        this.yearOfBirthday = 0;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setHumanSex(Sex sex) {
        this.sex = sex;
    }

    public void setYearOfBirthday(int yearOfBirthday) {
        this.yearOfBirthday = yearOfBirthday;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public String getName() {
        return this.name;
    }

    public String getSurname() {
        return this.surname;
    }

    public int getYearOfBirthday() {
        return this.yearOfBirthday;
    }

    public Sex getHumanSex() {
        return this.sex;
    }

    private int getIq() {
        return this.iq;
    }

    public Family getFamily() {
        return this.family;
    }

    abstract void greetPets();

    public void describePets() {
        this.getFamily().getPets().forEach(x -> {
            System.out.printf("I have a %s, it's %d age old, and it's %s tricky\n", x.getSpecie(), x.getAge(), x.getCunning());
        });
    }

    @Override
    public String toString() {
        return String.format("Human{" +
                "name='%s', " +
                "surname='%s', " +
                "year=%d, " +
                "iq=%d, ",
                this.name,
                this.surname,
                this.yearOfBirthday,
                this.iq
            );
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Human)) return false;

        Human x = (Human) obj;

        boolean nameEquality = this.getName().equals(x.getName());
        boolean surnameEquality = this.getSurname().equals(x.getSurname());
        boolean iqEquality = this.getIq() == x.getIq();

        return nameEquality && surnameEquality && iqEquality;
    }

    protected void finalize() {
        System.out.print(this);
    }
}
