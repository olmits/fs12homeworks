package homework8.family;

import homework8.family.member.Human;
import homework8.family.pet.Pet;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Family {
    private List<Human> children;
    private Human mother;
    private Human father;
    private Set<Pet> pets = new HashSet<>();

    public Family(Human mother, Human father) {

        this.children = new ArrayList<>();
        this.mother = mother;
        this.father = father;

        mother.setFamily(this);
        father.setFamily(this);
    }

    public void addChild(Human child) {
        this.children.add(child);
        child.setFamily(this);
    }

    public boolean deleteChild(int index) {
        if (index >= this.children.size()) return false;
        Human kidToDelete = this.children.get(index);
        kidToDelete.setFamily(null);

        this.children.remove(index);

        return true;
    }

    public boolean deleteChild(Human kid) {
        if (!this.children.contains(kid)) return false;
        kid.setFamily(null);
        this.children.remove(kid);
        return true;
    }

    public void addPet(Pet pet) {
        this.pets.add(pet);
    }

    public boolean deletePet(Pet pet) {
        if (!this.pets.contains(pet)) return false;
        this.pets.remove(pet);
        return true;
    }

    public int countFamily() {
        return 2 + this.children.size();
    }

    public void setMother(Human mother) {
        Human currentMother = this.mother;
        if (currentMother == mother) return;
        currentMother.setFamily(null);

        mother.setFamily(this);
        this.mother = mother;
    }

    public void setFather(Human father) {
        Human currentFather = this.father;
        if (currentFather == father) return;
        currentFather.setFamily(null);

        father.setFamily(this);
        this.father = father;
    }

    public List<Human> getChildren() {
        return this.children;
    }
    public Human getMother() {
        return this.mother;
    }
    public Human getFather() {
        return this.father;
    }
    public Set<Pet> getPets() {
        return this.pets;
    }

    @Override
    public String toString() {
        return String.format("Family{mother={%s %s}, father={%s %s}, children={%s}, pets={%s}}",
                this.mother.getName(),
                this.mother.getSurname(),
                this.father.getName(),
                this.father.getSurname(),
                this.children.stream()
                        .map(x -> String.format("%s %s", x.getName(), x.getSurname()))
                        .collect(Collectors.toList())
                        .toString(),
                this.pets.size() != 0
                        ? this.pets.stream()
                            .map(x -> String.format("%s %s", x.getSpecie(), x.getNickname()))
                        .collect(Collectors.toSet())
                        .toString()
                        : "no pet"
        );
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Family)) return false;

        Family x = (Family) obj;

        boolean dadEquality = this.getFather().equals(x.getFather());
        boolean momEquality = this.getMother().equals(x.getMother());

        return dadEquality && momEquality;
    }

    protected void finalize() {
        System.out.print(this);
    }
}
