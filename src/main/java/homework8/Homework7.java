package homework8;

import homework8.family.Family;
import homework8.family.member.Man;
import homework8.family.member.Woman;
import homework8.family.pet.Dog;
import homework8.family.pet.DomesticCat;

import java.util.Arrays;
import java.util.HashSet;

public class Homework7 {
    public static void main(String[] args) {
        Woman mom = new Woman("Sandra", "Green", 1937, 83);
        Man dad = new Man("Leonard", "Green", 1937, 83);
        Family family = new Family(mom, dad);

        Woman kid1 = new Woman("Rachel", "Green", 1969, 90);
        Woman kid2 = new Woman("Jill", "Green", 1974, 80);
        Woman kid3 = new Woman("Amy", "Green", 1979, 70);
        DomesticCat cat = new DomesticCat(
                "Whiskerson",
                10,
                90,
                new HashSet<>(Arrays.asList("scratch", "sunbathing")));
        Dog dog = new Dog(
                "Archie",
                5,
                60,
                new HashSet<>(Arrays.asList("being dirty", "being a good boy")));

        family.addChild(kid1);
        family.addChild(kid2);
        family.addChild(kid3);

        family.addPet(cat);
        family.addPet(dog);

        kid1.describePets();
        kid1.greetPets();
        cat.respond();
        dog.respond();
    }
}
