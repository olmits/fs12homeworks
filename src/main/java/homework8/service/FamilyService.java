package homework8.service;

import homework8.family.CollectionFamilyDao;
import homework8.family.Family;
import homework8.family.member.Human;
import homework8.family.member.Man;
import homework8.family.member.Woman;
import homework8.family.pet.Pet;

import java.util.List;
import java.util.Set;

public class FamilyService {

//    TODO: why FamilyDao should be here instead of CollectionFamilyDao?
    private CollectionFamilyDao collectionFamilyDao = new CollectionFamilyDao();

    public List<Family> getAllFamilies() {
        return collectionFamilyDao.getAllFamilies();
    }

    public void getFamiliesBiggerThan(int number) {
        collectionFamilyDao.getFamiliesBiggerThan(number);
    }

    public void getFamiliesLessThan(int number) {
        collectionFamilyDao.getFamiliesLessThan(number);
    }

    public void createNewFamily(Woman woman, Man man) {
        collectionFamilyDao.createNewFamily(woman, man);
    }

    public void bornChild(Family family, String boyName, String girlName) {
        collectionFamilyDao.bornChild(family, boyName, girlName);
    }

    public void adoptChild(Family family, Human kid) {
        collectionFamilyDao.adoptChild(family, kid);
    }

    public void deleteAllChildrenOlderThen(int age) {
        collectionFamilyDao.deleteAllChildrenOlderThen(age);
    }

    public int count() {
        return collectionFamilyDao.count();
    }

    public Set<Pet> getPets(long id) {
        return collectionFamilyDao.getPets(id);
    }

    public void addPet(long id, Pet pet) {
        collectionFamilyDao.addPet(id, pet);
    }
}
