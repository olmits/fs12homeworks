package homework8.dao;

import homework8.family.Family;

import java.util.List;

public interface FamilyDao {
    List<Family> getAllFamilies();

    Family getFamilyByIndex(long id);

    boolean deleteFamily(long id);

    boolean deleteFamily(Family family);

    void saveFamily(Family family);
}
