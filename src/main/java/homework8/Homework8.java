package homework8;

import homework8.controller.FamilyController;
import homework8.family.Family;
import homework8.family.member.Man;
import homework8.family.member.Woman;
import homework8.family.pet.Dog;
import homework8.family.pet.DomesticCat;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

public class Homework8 {
    public static void main(String[] args) {
        Woman woman1 = new Woman("Sandra", "Green", 1937, 83);
        Woman woman2 = new Woman("Dolly", "Hunter", 1938, 84);
        Woman woman3 = new Woman("Deon", "Hood", 1939, 85);
        Woman woman4 = new Woman("Jeanne", "Meza", 1940, 86);

        Man man1 = new Man("Leonard", "Green", 1939, 77);
        Man man2 = new Man("Caolan", "Hunter", 1940, 64);
        Man man3 = new Man("Taliyah", "Hood", 1941, 89);
        Man man4 = new Man("Archie", "Meza", 1942, 57);

        Woman kid1 = new Woman("Rachel", "Green", 1979, 90);
        Woman kid2 = new Woman("Jill", "Green", 1984, 80);
        Woman kid3 = new Woman("Amy", "Green", 1989, 70);
        DomesticCat cat = new DomesticCat(
                "Whiskerson",
                10,
                90,
                new HashSet<>(Arrays.asList("scratch", "sunbathing")));
        Dog dog = new Dog(
                "Archie",
                5,
                60,
                new HashSet<>(Arrays.asList("being dirty", "being a good boy")));

        FamilyController familyController = new FamilyController();
        familyController.createNewFamily(woman1, man1);
        familyController.createNewFamily(woman2, man2);
        familyController.createNewFamily(woman3, man3);
        familyController.createNewFamily(woman4, man4);
        System.out.printf(">>> Amount of families in db: %d\n", familyController.count());

        List<Family> families = familyController.getAllFamilies();
        Family family1 = families.get(0);
        Family family2 = families.get(1);

        familyController.adoptChild(family1, kid1);
        familyController.adoptChild(family1, kid2);
        familyController.adoptChild(family1, kid3);
        familyController.bornChild(family2, "Donald", "Annie");
        familyController.bornChild(family2, "Alex", "Jeannie");
        familyController.addPet(2, cat);
        familyController.addPet(2, dog);

        familyController.getFamiliesBiggerThan(3);
        familyController.getFamiliesLessThan(3);
        familyController.getPets(2).forEach(System.out::println);

        familyController.deleteAllChildrenOlderThen(32);
        families.stream()
                .flatMap(x -> x.getChildren().stream())
                .collect(Collectors.toList())
                .forEach(System.out::println);


    }
}
