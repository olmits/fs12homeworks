package homework8;

import homework8.family.member.Man;

public class Homework5 {
    public static void main(String[] args) {
        long count = 10000;
        while (count > 0) {
             --count;
            new Man("Name", "Surname", 1992);
        }
    }
}
