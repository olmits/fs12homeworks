package homework8;

import homework8.family.Family;
import homework8.family.member.Man;
import homework8.family.member.Woman;
import homework8.family.pet.DomesticCat;

import java.util.Arrays;
import java.util.HashSet;

public class Homework6 {
    public static void main(String[] args) {
        Woman mom = new Woman("Sandra", "Green", 1937, 83);
        Man dad = new Man("Leonard", "Green", 1937, 83);
        Family family = new Family(mom, dad);

        Woman kid = new Woman("Rachel", "Green", 1069, 51);
        DomesticCat cat = new DomesticCat("the Cat");

        family.addChild(kid);
        family.addPet(cat);

        cat.setNickname("Whiskerson");
        cat.setAge(10);
        cat.setCunning(99);
        cat.setHabits(new HashSet<>(Arrays.asList("to scratch", "to hurt", "to bring an Apocalypse")));

        System.out.printf("HAPPY FAMILY: %s\n", family);

        dad.repairCar();
        mom.makeUp();
        kid.greetPets();
        cat.respond();
        cat.eat();
        cat.fool();
    }
}
