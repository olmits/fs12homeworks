package homework8.controller;

import homework8.family.Family;
import homework8.family.member.Human;
import homework8.family.member.Man;
import homework8.family.member.Woman;
import homework8.family.pet.Pet;
import homework8.service.FamilyService;

import java.util.List;
import java.util.Set;

public class FamilyController {
    public FamilyService familyService = new FamilyService();

    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }

    public void getFamiliesBiggerThan(int number) {
        familyService.getFamiliesBiggerThan(number);
    }

    public void getFamiliesLessThan(int number) {
        familyService.getFamiliesLessThan(number);
    }

    public void createNewFamily(Woman woman, Man man) {
        familyService.createNewFamily(woman, man);
    }

    public void bornChild(Family family, String boyName, String girlName) {
        familyService.bornChild(family, boyName, girlName);
    }

    public void adoptChild(Family family, Human kid) {
        familyService.adoptChild(family, kid);
    }

    public void deleteAllChildrenOlderThen(int age) {
        familyService.deleteAllChildrenOlderThen(age);
    }

    public int count() {
        return familyService.count();
    }

    public Set<Pet> getPets(long id) {
        return familyService.getPets(id);
    }

    public void addPet(long id, Pet pet) {
        familyService.addPet(id, pet);
    }
}
