package homework3;

public enum Days {
    SUNDAY (0),
    MONDAY (1),
    TUESDAY (2),
    WEDNESDAY (3),
    THURSDAY (4),
    FRIDAY (5),
    SATURDAY (6);

    private int index;
    Days(int index) {
        this.index = index;
    }

    public int getIndex() { return index; }
}
