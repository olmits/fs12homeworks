package homework3;

import java.util.Arrays;
import java.util.Scanner;

public class Homework3 {

    private static Scanner scanner = new Scanner(System.in);

    private static void print(String s) {
        System.out.print(s);
    }

    private static boolean isExit(String s) {
        boolean result = s.equals("exit");
        running = !result;
        return result;
    }

    private static String[] schedule = new String[7];
    private static Days currentDay;
    private static int currentDayIndex;
    private static boolean running = true;
    private static boolean rescheduling = false;

    /**
     * Check if string has a valid number of substrings and means rescheduling
     * If line contains 1 substring - just return this string
     * If line contains 2 substrings - divide string and check if first substring means rescheduling
     * If line contains 3 or more substring - return empty string
     * @param s - String to format
     * @return formatted String
     */
    private static String formatAndCheckRescheduling(String s) {
        String [] array = s.split(" ");
        switch (array.length){
            case 1: return array[0].toUpperCase();
            case 2: {
                String leftPart = array[0].toLowerCase();
                rescheduling = leftPart.equals("reschedule") || leftPart.equals("change");
                return array[1].toUpperCase();
            }
            default: return "";
        }
    }

    /**
     * Read a line and verify if line contains valid day of week
     * For validating enum Days is used
     * If line equals to "exit", exit running
     * If line valid, place a Day to "currentDay" and Day index to "currentDayIndex"
     * @param sc - Scanner used to read a line
     */
    private static void waitForDayInput(Scanner sc) {
        try {
            print("\nPlease, input the day of the week: ");
            String line = sc.nextLine().trim();

            if (isExit(line)) return;

            String lineFormatted = formatAndCheckRescheduling(line);
            currentDay = Days.valueOf(lineFormatted);
            currentDayIndex = currentDay.getIndex();
        }
        catch (IllegalArgumentException ex) {
            print("Sorry, I don't understand you, please try again.\n");
            waitForDayInput(sc);
        }
    }

    /**
     * Read a line and place this line to schedule array
     * To choose an index to place, use currentDayIndex
     * If line equals to "exit", exit running
     * @param sc - Scanner used to read a line
     */
    private static void waitForRescheduling(Scanner sc) {
        print(String.format("\nPlease, input new tasks for %s: ", currentDay.name()));
        String line = sc.nextLine().trim();

        if (isExit(line)) return;

        schedule[currentDayIndex] = line;
    }

    public static void main(String[] args) {
        Arrays.fill(schedule, "N/A");
        while (running) {
            waitForDayInput(scanner);
            if (!running) break;

            if (rescheduling) {
                waitForRescheduling(scanner);
            }

            System.out.printf("Your tasks for %s: %s\n", currentDay.name(), schedule[currentDayIndex]);
            rescheduling = false;
        }
    }
}
